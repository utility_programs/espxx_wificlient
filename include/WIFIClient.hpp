/**
 * @file WIFIClient.hpp
 * @author Julian Neundorf
 * @brief
 * @version 0.1
 * @date 2024-01-04
 *
 * Remarks:
*/
#pragma once

#include <array>
#include <cstdint>

#include <StateMachine.hpp>
#include <TaskHandler.hpp>

enum class ConnectionStatus {
  Disconnected = 0,
  Connect,
  Connecting,
  Connected,
  Reset,
  Disconnect
};

class WIFIClient : private Task, public StateMachine<ConnectionStatus> {
public:
  static WIFIClient& getInstance() {
    static WIFIClient wifiClient;
    return wifiClient;
  }

  void activate(const std::array<char, 33>& SSID, const std::array<char, 64>& Key, const std::array<char, 20>& mDNS);
  void activate();
  void deactivate();

private:
  void run_() override;

  WIFIClient();

  ConnectionStatus connectionStatus = ConnectionStatus::Disconnected;
  std::uint32_t lastConnectionStatusChange = 0;

  std::array<char, 33> ssid;
  std::array<char, 64> key;
  std::array<char, 20> mdns;
};
