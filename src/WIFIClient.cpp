/**
 * @file WIFIClient.cpp
 * @author Julian Neundorf
 * @brief
 * @version 0.1
 * @date 2024-01-04
 *
 * Remarks:
*/

#include "WIFIClient.hpp"

#include <array>
#include <ESPAsyncWebServer.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPClient.h>

static constexpr std::uint32_t checkWifiIntervalMilli = 1000;

void HandleNotFound(AsyncWebServerRequest* request);
void HandleReboot(AsyncWebServerRequest* request);

AsyncWebServer server(80);

void HandleReboot(AsyncWebServerRequest* request) {
  request->send(200);
  Serial.println("Restarting...");
  ESP.restart();
}

void HandleNotFound(AsyncWebServerRequest* request) {
  request->redirect("/?error=notfound");
}

WIFIClient::WIFIClient() : Task(checkWifiIntervalMilli) {

}

void WIFIClient::activate(const std::array<char, 33>& SSID, const std::array<char, 64>& Key, const std::array<char, 20>& mDNS) {
  std::copy(SSID.begin(), SSID.end(), ssid.begin());
  std::copy(Key.begin(), Key.end(), key.begin());
  std::copy(mDNS.begin(), mDNS.end(), mdns.begin());
  Serial.printf("Connecting with WIFI: '%s'\n", SSID.data());
  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID.data(), Key.data());

  server.onNotFound(HandleNotFound);
  server.begin();
  server.on("/reboot", HTTP_GET, HandleReboot);

  setStatus(ConnectionStatus::Connect);
}

void WIFIClient::run_() {
  ConnectionStatus status = getStatus();

  switch (status) {
  case ConnectionStatus::Disconnected:
    Serial.println("Disconnected");
    setDelayMillis(1000);
    return;
  case ConnectionStatus::Connect:
    Serial.println("Connect");
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid.data(), key.data());
    setStatus(ConnectionStatus::Connecting);
    return;
  case ConnectionStatus::Connecting:
    Serial.println("Connecting");
    if (WiFi.waitForConnectResult() == WL_CONNECTED) {
      setStatus(ConnectionStatus::Connected);
      Serial.print("IP number assigned by DHCP is ");
      Serial.println(WiFi.localIP());
      WiFi.setAutoReconnect(true);
      WiFi.persistent(true);

      if (MDNS.begin(mdns.begin())) {
        Serial.printf("Started mDNS: %s.local\n", mdns.begin());
      }
      return;
    } else {
      Serial.println("Failing connecting...");
      if (getDiffLastChange() > 3000) // Reset connection
        setStatus(ConnectionStatus::Reset);
    }
    return;
  case ConnectionStatus::Connected:
    Serial.println("Connected");
    if (WiFi.waitForConnectResult() != WL_CONNECTED)
      setStatus(ConnectionStatus::Reset);
    return;
  case ConnectionStatus::Reset:
    Serial.println("Reset");
    WiFi.disconnect(true);
    setStatus(ConnectionStatus::Connect);
    return;
  case ConnectionStatus::Disconnect:
    Serial.println("Disconnect");
    WiFi.disconnect(true);
    setStatus(ConnectionStatus::Disconnected);
    return;
  default:
    Serial.println("Unknown state! Reset state");
    setStatus(ConnectionStatus::Disconnected);
    Serial.printf("Status: %u\n", (uint8_t)getStatus());
    return;
  }
}

void WIFIClient::activate() {
  if (getStatus() == ConnectionStatus::Disconnected)
    setStatus(ConnectionStatus::Connect);
}

void WIFIClient::deactivate() {
  setStatus(ConnectionStatus::Disconnect);
}